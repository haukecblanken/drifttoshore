#folder with IOSshell format drifter data (only needed in you don't have a data/waterproperties_drifter_data.pickle file)
drifter_data_folder = 'C:/Users/hauke/Documents/DFO/ZimKingston/drifter_data/'

#spacing of points along user-defined line to check drifter proximity at, likely don't need to change this unless you make line_thres very small
line_res = 500.

#minimum distance a drifter must be to specified line in order to be counted
line_thres = 2000.

#distance to coast within which a drifter is considered to be grounded
coast_thres = 500.

#height of map of drifter tracks in inches
map_height = 5.

#define map projection to use for conversion to UTM, don't need to change this unless the analysis area is well away from the BC coast (outside zone 9)
from pyproj import Proj
proj = Proj("+proj=utm +zone=9 +north +ellps=WGS84 +datum=WGS84 +units=m +no_defs")
