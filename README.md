# DriftToShore

Small project to determine time taken for surface drifters from the IOS waterproperties data archive to reach the shore from a user-defined line (for example, shipping lanes).

## Installation
Requires a working python environment with numpy, matplotlib, basemap, and pyproj. Clone or download the source code and run within this environment. An installation via setuptools is neither available, nor planned.

## Usage
From the main project folder, enter the parameters for your analysis in namelist.py. Then run 'python drifttoshore.py'. The program will ask you to draw the line from which you wish to track observed drifters, either by drawing it on a map (d) or by inputting the coordinates manually (m). Follow the instructions on the command line, and the program will find all drifters that travelled within the specified distance of your line (line_thres in namelist.py) and the time they took to reach the shore (again within the specified distance, coast_thres). A map of the relevant drifters and a histogram of the time they took to reach the shore will then be saved to the main project folder.

## Support
Please open an issue in the tracker if you run into problems.

## Contributing
If you're interested in contributing, please submit a merge request with your proposed changes.

## Authors and acknowledgment
Hauke Blanken, Ocean Sciences Division of Fisheries and Oceans Canada, Ocean Modelling and Predictions Section
Many thanks to everyone in DFO, CCG, and other organizations who has contributed to collecting the drifter data in waterproperties.ca

## License
Apache 2.0

## Project status
Completed for the time being, may update in the future with added functionality as needed.
