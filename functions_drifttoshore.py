from os import path
import pickle
import numpy as np
import datetime
import glob
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

from namelist import *

def read_drifter_data(target_folder,proj):
    picklename = './data/waterproperties_drifter_data.pickle'
    if path.exists(picklename):
        print('Loading drifter data from %s'%picklename)
        with open(picklename,'rb') as fid:
            drifters = pickle.load(fid)
    else:
        print('Reading drifter data from IOSshell files in %s'%target_folder)
        epoch = datetime.datetime(1970,1,1,0,0,0)
        files = sorted(glob.glob(path.join(target_folder,'*.drf')))
        drifters = {}
        for f in files:
            with open(f) as fid:
                lines = [line for line in fid]

            for i,line in enumerate(lines):
                if 'MODEL' in line:
                    buoy_type = line.split(':')[-1].replace(' ','')[:-2]
                if 'SCIENTIST' in line:
                    PI = line.split(':')[-1].replace(' ','')[:-2]
                if 'DEVICE ID' in line:
                    buoy_id = int(line.split(' ')[-1].replace(' ',''))
                if 'END OF HEADER' in line:
                    skip_head = i + 1
                    break

            datestr = np.genfromtxt(f,dtype = 'str',usecols = 1,skip_header = skip_head)
            if np.size(datestr) < 2:
                continue
            timestr = np.genfromtxt(f,dtype = 'str',usecols = 2,skip_header = skip_head)
            time = np.array([datetime.datetime.strptime(d + ' ' + t,'%Y/%m/%d %H:%M:%S') for d,t in zip(datestr,timestr)])
            time_seconds = np.array([(t - epoch).total_seconds() for t in time])

            lat = np.genfromtxt(f,dtype = 'float',usecols = 3,skip_header = skip_head)
            lon = np.genfromtxt(f,dtype = 'float',usecols = 4,skip_header = skip_head)
            qc = np.genfromtxt(f,dtype = 'int',usecols = 5,skip_header = skip_head)

            cq = np.where(qc == 1)[0]
            if len(cq) < 5:
                continue
            time = time[cq]
            time_seconds = time_seconds[cq]
            lat = lat[cq]
            lon = lon[cq]
            x,y = proj(lon,lat)

            drifters[buoy_id] = {'lat':lat,'lon':lon,'x':x,'y':y,'time':time,'type':buoy_type}

        with open(picklename,'wb',-1) as fid:
            pickle.dump(drifters,fid)

    return drifters

def make_basemap(drifters,proj):
    basemapfile = './data/basemap_drifter_data.pickle'
    if path.exists(basemapfile):
        print('loading coastline data from %s'%basemapfile)
        with open(basemapfile,'rb') as fid:
            data = pickle.load(fid)
            polystack = data['stack']
            mpolys = data['map']
    else:
        print('generating coastline data from GSHHS data set')
        lats = [10**36,-10**36]
        lons = [10**36,-10**36]

        for did in drifters.keys():
            lats[0] = np.min([lats[0],drifters[did]['lat'].min()])
            lats[1] = np.max([lats[1],drifters[did]['lat'].max()])
            lons[0] = np.min([lons[0],drifters[did]['lon'].min()])
            lons[1] = np.max([lons[1],drifters[did]['lon'].max()])

        m = Basemap(projection='merc',resolution='f',llcrnrlat=lats[0],llcrnrlon=lons[0],urcrnrlat=lats[1],urcrnrlon=lons[1])
        mpolys = [m(p[0],p[1],inverse=True) for p in m.coastpolygons]
        polys = [proj(p[0],p[1]) for p in mpolys]
        polystack = np.vstack([np.asarray(p).T for p in polys])

        with open(basemapfile,'wb',-1) as fid:
            pickle.dump({'stack':polystack,'map':mpolys},fid)

    return polystack,mpolys

def get_line(mpolys):
    manual_switch = input('Would you like to input points manually (m) or draw them on a map (d): ')
    if manual_switch == 'm':
        ui = None
        pts = []
        while ui != 'q':
            ui = input('Please input point (lon,lat) or terminate line (q): ')
            if ui != 'q':
                try:
                    pts.append(ui.split(','))
                except:
                    print('Please use appropriate input format (decimal values separated by a comma)')
        pts = np.asarray(pts).astype(float)

        return pts
    elif manual_switch == 'd':
        print('Double-click to define the points in your line, and press q or close this window once finished. Press d to delete the last point.')
        pts = []
        line = None
        fig,ax = plt.subplots(figsize=(10,8))
        for p in mpolys:
            ax.plot(p[0],p[1],color='k')

        def on_click(event,pts,line):
            if event.dblclick:
                ix, iy = event.xdata, event.ydata
                pts.append([ix,iy])
                if line is not None:
                    line.remove()
                line = ax.plot(np.asarray(pts)[:,0],np.asarray(pts)[:,1],color='r',marker='o')
                plt.draw()

        def on_press(event,pts,line):
            if event.key == 'q':
                plt.close()
                return np.asarray(pts)
            elif event.key == 'd':
                pts = np.asarray(pts)[:-1,:].tolist()
                if line is not None:
                    line.remove()
                line = ax.plot(np.asarray(pts)[:,0],np.asarray(pts)[:,1],color='r',marker='o')
                plt.draw()

        fig.canvas.mpl_connect('button_press_event', lambda event: on_click(event, pts, line))
        fig.canvas.mpl_connect('key_press_event', lambda event: on_press(event, pts, line))

        plt.show()

        return np.asarray(pts)
    else:
        raise ValueError('Only m and d are valid inputs. Please retry.')

def format_pts(pts,res,proj):
    line_xy = proj(pts[:,0],pts[:,1])
    length = np.sqrt(np.diff(line_xy[0])**2. + np.diff(line_xy[1])**2.)
    N = int(np.ceil(np.sum(length)/res))
    lxs = np.hstack([np.linspace(line_xy[0][i],line_xy[0][i+1],int(N*length[i]/np.sum(length))) for i in range(len(length))])
    lys = np.hstack([np.linspace(line_xy[1][i],line_xy[1][i+1],int(N*length[i]/np.sum(length))) for i in range(len(length))])
    line = np.column_stack([lxs,lys])

    return line

def get_line_drifters(drifters,line,line_thres):
    crossed_line = []
    for did in drifters.keys():
        N = len(drifters[did]['lon'])
        d = np.zeros([N,line.shape[0]])
        for j in range(N):
            d[j,:] = np.sqrt((line[:,0] - drifters[did]['x'][j])**2. + (line[:,1] - drifters[did]['y'][j])**2.)
        j,i = np.where(d < line_thres)
        if len(j) == 0:
            continue
        if np.logical_and(d.min() < line_thres,len(drifters[did]['lon'][j[-1]:]) > 5):
            crossed_line.append([int(did),j[-1],d[j[-1],i[-1]]])

    return crossed_line

def get_grounded_drifters(drifters,coast_thres,polystack,crossed_line):
    crossed_and_grounded = []
    for i in range(len(crossed_line)):
        did = crossed_line[i][0]
        si = crossed_line[i][1]
        N = len(drifters[did]['lon'][si:])
        for j in range(N):
            d = np.sqrt((polystack[:,0] - drifters[did]['x'][si:][j])**2. + (polystack[:,1] - drifters[did]['y'][si:][j])**2.)
            if d.min() < coast_thres:
                ei = si + j
                time_to_ground = (drifters[did]['time'][ei] - drifters[did]['time'][si]).total_seconds() / 86400.
                crossed_and_grounded.append([did,si,ei,time_to_ground])
                grounded = True
                break

    return crossed_and_grounded

def plot_map(crossed_and_grounded,drifters,mpolys,line_pts,case,map_height):
    fig,ax = plt.subplots()
    for p in mpolys:
        ax.plot(p[0],p[1],color='k')

    ax.plot(line_pts[:,0],line_pts[:,1],color='r',marker='o')

    buff = 0.1
    latlims = [10**36,-10**36]
    lonlims = [10**36,-10**36]
    for i in range(len(crossed_and_grounded)):
        did = crossed_and_grounded[i][0]
        si = crossed_and_grounded[i][1]
        ei = crossed_and_grounded[i][2]
        tg = crossed_and_grounded[i][3]
        ax.plot(drifters[did]['lon'][si:ei],drifters[did]['lat'][si:ei],color='b')
        if len(drifters[did]['lat'][si:ei]) > 0:
            latlims = [np.min([latlims[0],drifters[did]['lat'][si:ei].min()]),np.max([latlims[1],drifters[did]['lat'][si:ei].max()])]
            lonlims = [np.min([lonlims[0],drifters[did]['lon'][si:ei].min()]),np.max([lonlims[1],drifters[did]['lon'][si:ei].max()])]

    ax.set_ylim(latlims[0] - buff,latlims[1] + buff)
    ax.set_xlim(lonlims[0] - np.cos(latlims[0] * np.pi/180.) * buff,lonlims[1] + np.cos(latlims[0] * np.pi/180.) * buff)

    ax.grid(which='both')
    ax.set_xlabel('Longitude')
    ax.set_ylabel('Latitude')

    if map_height * np.cos(latlims[0] * np.pi/180.) * (lonlims[1] - lonlims[0]) > map_height:
        mx = map_height
        my = map_height / np.cos(latlims[0] * np.pi/180.)
    else:
        mx = map_height * np.cos(latlims[0] * np.pi/180.) * (lonlims[1] - lonlims[0])
        my = map_height
    fig.set_size_inches(mx,my)

    plt.savefig('./output/drifter_map_%s.jpg'%case,dpi=450,bbox_inches='tight')
    plt.show()

def plot_histogram(crossed_and_grounded,case):
    tg = np.asarray(crossed_and_grounded)[:,-1]
    fig,ax = plt.subplots(figsize=(5,5))
    _ = ax.hist(tg,facecolor='0.8',edgecolor='k')

    ax.axvline(tg.mean(),color='k',ls='--')
    ax.axvline(tg.mean() - tg.std(),color='k',ls='dotted')
    ax.axvline(tg.mean() + tg.std(),color='k',ls='dotted')

    ax.text(0.98,0.98,r'Mean time to shore= %.1f $\pm$ %.1f days'%(tg.mean(),tg.std()),ha='right',va='top',transform=ax.transAxes)

    ax.set_xlabel('Time to Shore (days)')
    ax.set_ylabel('Number of drifters')

    ax.grid(which='both')

    plt.savefig('./output/time_to_shore_histogram_%s.jpg'%case,dpi=450,bbox_inches='tight')
    plt.show()
