from namelist import *
from functions_drifttoshore import *

#read in drifter data
drifters = read_drifter_data(drifter_data_folder,proj)
#read in coastline data
polystack,mpolys = make_basemap(drifters,proj)
#define a line to check drifters from
pts = get_line(mpolys)
line = format_pts(pts,line_res,proj)
print('Finding drifters that have crossed this line and subsequently grounded...')
#find all drifters that have crossed the defined line
crossed_line = get_line_drifters(drifters,line,line_thres)
#find all drifters that have crossed the defined line and then grounded out
crossed_and_grounded = get_grounded_drifters(drifters,coast_thres,polystack,crossed_line)
print('Found %1i relevant drifters'%len(crossed_and_grounded))
#input case name for record keeping
case = input('Please input a case name for your line: ')
#plot a map of the defined line and the relevant drifter tracks
plot_map(crossed_and_grounded,drifters,mpolys,pts,case,map_height)
#plot histogram of times taken by drifters to travel from specified line to shore
plot_histogram(crossed_and_grounded,case)
